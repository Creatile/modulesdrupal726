1)		Download the plugin (avaitable at the bottom of this article). 
		/!\ Don’t forget to extract the content of the file when saving it. /!\

2)		Copy the folder ‘aza_tracker’ into the Drupal plugin folder.
		The Drupal plugin folder can be found on the Drupal root directory : Drupal > Modules
		Once you are into the Modules folder, you can copy/paste the ‘aza_tracker’ folder there (Example : Next to the README.txt file)

3)		When you’re done, login as administrator to your website and click on ‘Modules’ in the top menu bar.

4)		The Azalead tracker module will appear in the block ‘Other’ at the bottom of your page. 
		Once you have found it, enable it by ticking the box and then click on Save Configuration. 


5)		The plugin is enabled. Go to ‘Configurations’ (in the top menu bar).
		Look for the block ‘Development’ and click on ‘Performance’. 
		In order to start the plugin you need to clear your cache. Click on the button ‘Clear all caches’. 

6)		Return to the ‘Modules’ pages (step 3/4). Scroll down to the Azalead Tracker plugin and click on the link ‘Configure’. 


7)		You will now add the azalead tag ID received by email. Fill the form and click on Save Configuration.

>>	Return to your website, refresh the page and it works ! <<
